[Indonesia -> Sunda]

banyak -> loba, halus:réa, halus:seueur
bau -> halus:angseu, bau, ambeu
beda -> béda, halus:bénten
bersih -> resik, beresih
besar -> gedé, halus:ageung
bukan -> sanés
cepat -> gancang, halus:enggal
dingin -> tiis, tiris
gelap -> poék
kecil ->  leutik, halus:alit
kotor -> kotor, goréng
kurang -> kurang, halus:kirang
lambat -> alon, laun, kendor
lebih -> leuwih, halus:langkung
mirip -> sarua
muda -> ngora, halus:anom
panas -> panas
panjang -> panjang, halus:paos
pendek -> pendék
rendah -> handap
sama -> sarua, halus:sami
sedang -> halus:cekap, sedeng
sedikit -> saeutik, halus:saalit
terang -> caang
tinggi -> luhur, muluk
tua -> kolot, halus:sepuh
wangi -> wangi, seungit