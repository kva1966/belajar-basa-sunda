# Belajar Basa Sunda
Berbagai catatan untuk belajar Basa Sunda bagi yang sudah menguasai Bahasa Indonesia. Catatan dibuat penyusun selama penyusun sendiri mempelajari Basa Sunda secara informal. Dengan demikian, lumrah kemungkinan adanya kesalahan serta ketidaklengkapan. 

Repositori ini disediakan dengan izin (_permission_) umum dengan lisensi Creative Commons 3.0, tepatnya CC-BY-NC-SA dengan harapan bisa membantu orang lain yang sedang belajar, dan juga sebagai wadah penyusun untuk menerima bantuan/koreksi/masukan dari pembaca yang lebih fasih berbahasa Sunda.

Mangga me-_fork_, lalu mengkoreksi atau menambah, dan mengajukan _pull request_. Hatur Nuhun.

# Lisensi
![Alt text](http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png)

Teks Lisensi CC-BY-NC-SA:

* [Bahasa Inggris](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_GB)
* [Bahasa Indonesia](http://wiki.creativecommons.org/Licenses/by-nc-sa/3.0LegalText_(Indonesian))

